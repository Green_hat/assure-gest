import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {AssureModule} from './assure/assure.module';
import {AssureComponent} from './assure/assure.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AssureModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
