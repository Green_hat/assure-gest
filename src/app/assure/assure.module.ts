import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FicheAssureComponent} from './fiche-assure/fiche-assure.component';
import {AssureComponent} from './assure.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {AssureService} from './assure.service';
import {ListeAssureComponent} from './liste-assure/liste-assure.component';
import {FormAssureComponent} from './form-assure/form-assure.component';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    FicheAssureComponent,
    AssureComponent,
    ListeAssureComponent,
    FormAssureComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule
  ],
  exports: [
    AssureComponent
  ],
  providers: [AssureService, HttpClient]
})
export class AssureModule { }
