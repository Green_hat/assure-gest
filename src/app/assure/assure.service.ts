import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Assure} from './assure';

@Injectable({
  providedIn: 'root'
})
export class AssureService {

  constructor(private http: HttpClient) { }

  findOne(id: number): Observable<Assure> {
    return this.http.get<Assure>('http://127.0.0.1:8080/assure/' + id);
  }

  findAll(): Observable<Assure[]> {
    return this.http.get<Assure[]>('http://127.0.0.1:8080/assure/');
  }
}
