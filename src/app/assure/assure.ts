export class Assure {
  constructor(
    public nom: string,
    public prenom: string,
    public matricule: number,
    public date: Date,
    public salaire: number,
    public employeurId?: number,
    public id?: number
  ) { }
}
