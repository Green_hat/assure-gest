import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FicheAssureComponent } from './fiche-assure.component';

describe('FicheAssureComponent', () => {
  let component: FicheAssureComponent;
  let fixture: ComponentFixture<FicheAssureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FicheAssureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FicheAssureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
