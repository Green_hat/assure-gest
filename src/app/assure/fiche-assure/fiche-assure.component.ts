import {Component, Input, OnInit} from '@angular/core';
import {Assure} from '../assure';
import {AssureService} from '../assure.service';

@Component({
  selector: 'app-fiche-assure',
  templateUrl: './fiche-assure.component.html',
  styleUrls: ['./fiche-assure.component.css']
})
export class FicheAssureComponent implements OnInit {
  @Input()  public assure: Assure;
  constructor(private assureService: AssureService) { }

  ngOnInit() {

  }

}
