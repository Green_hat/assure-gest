import { Component, OnInit } from '@angular/core';
import {Assure} from '../assure';

@Component({
  selector: 'app-form-assure',
  templateUrl: './form-assure.component.html',
  styleUrls: ['./form-assure.component.css']
})
export class FormAssureComponent implements OnInit {
public assure: Assure;
public active: boolean;
  constructor() {
    this.assure = new Assure( '', '', 0, new Date(), 0);
    this.active = false;
  }

  ngOnInit() {
  }

  toggle() {
    this.active = !this.active;
  }
}
