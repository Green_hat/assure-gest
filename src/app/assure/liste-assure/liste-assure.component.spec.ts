import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeAssureComponent } from './liste-assure.component';

describe('ListeAssureComponent', () => {
  let component: ListeAssureComponent;
  let fixture: ComponentFixture<ListeAssureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListeAssureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeAssureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
