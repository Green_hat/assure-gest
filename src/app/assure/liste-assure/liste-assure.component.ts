import {Component, Input, OnInit} from '@angular/core';
import {AssureService} from '../assure.service';
import {Assure} from '../assure';

@Component({
  selector: 'app-liste-assure',
  templateUrl: './liste-assure.component.html',
  styleUrls: ['./liste-assure.component.css']
})
export class ListeAssureComponent implements OnInit {
  public  assure: Assure;
  public liste: Assure[];
  constructor(private assureService: AssureService) { }

  ngOnInit() {
    this.assureService.findAll().subscribe(liste => this.liste = liste);
  }

  afficher(id: number) {
    this.assureService.findOne(id).subscribe(ass => this.assure = ass);
  }
}
