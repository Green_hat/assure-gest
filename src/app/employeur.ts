export class Employeur {
  constructor(
    public rais: string,
    public adresse: string,
    public fisc: string,
    public matricule: number,
    public id?: number
  ) { }
}
